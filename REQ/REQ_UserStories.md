As of now (18/10/2021), the team PlateBoard has the following requirements under the conventions of a **user story**:

- **As a** project manager, **it is important to** know the dimensions of the changes on each commit, **in order to** know if each developer is committing his work regularly and to know how much are the reviewers needing to correct in the developers' work.

- **As a** project manager, **I want** login to be required for accessing my project, **in order to** keep the code safe from intruders and to avoid plagiarism from the competition.

- **As a** user, **I want to** see total effort applied in each category I**n order to** know priority distribution.

- **As a** developer, **I want** events on the timeline to be organized by commit instants, **in order to** have a better organization of the events and to display the most recent ones first, which should be the most relevant.

- **As a** project manager, **it is important to** be able to visualize the evolution of the effort applied by each member by category (REQ, PM, DEV...), **in order** to know what each person prefers to work on.

- **As a** developer, **it is important for** team members to view all issues' timeline (open, active, and total) **in order to** know about what has been done/being done. This is important because members may need to know details about tasks that have been done/are being done.

- **As a** developer, **it is important for** team members to know and see what type (REQ, TST, …) the file is, size (at that moment), stability metrics, and if code, its complexity, **in order to** know if the file has changed and for the team members to know the information/characteristics of the file that they are working on.

- **As a** developer, **it is important for** all the team members to navigate through the views of the project **in order to** monitor project development and the involvement of all team members in project tasks.

- **As a** developer, **it is important for** the team members to know which are/were the active/open issues at any given time **in order to** have knowledge of what is being done/has been done. This is important because members may need to know if they have to wait for some tasks to be finished or if they can start their tasks.

- **As a** developer, **I want to** see the list of contributions by category (REQ, PM, DEV, TST…) of each active member, **in order to** have a better understanding of how the work is being distributed.

- **As a** developer, **it is important for** the project members need to be able to see the list containing all the commits of a given file **in order to** know who changed it at any given time.

- **As a** developer, **I want to** visualize each team member commit's timeline and which files were modified in each commit, **in order to** have a better understanding of the workflow and each member's performance.

- **As a** project/team manager, **I want to** list each commit of my team's members, **in order to** know how each member is working and possibly change how the tasks are being distributed.

- **As a** user of the dashboard, **it's very important to** have an easy way of going back to the previous view without having to reinsert the options that lead to that specific view, **in order to** speed up the workflow.

- **As a** user of the dashboard, **I need to** choose the repository I want to access, **in order to** only get the information I'm looking for.

- **As a** developer, **it is important for** the team to be able to distinguish each file category **in order to** capture as much information as possible without great effort. So, by differentiating the categories, we optimize the workflow.

- **As a** developer, **it is important to** have the contributions grouped by category, **in order to** filter what the developers need to see at that moment.

- **As a** developer, **it is important to** know what bugs there are in a file, **in order to** help those who will correct them speed up their work and avoid two people working on the same issue by mistake.

- **As a** developer, **it is important for** the team members to know which category is representing a contribution at any given time **in order** to know about what is being done/has been in each category. This is important because members may need to know how the work is progressing in all different categories. Possible tests are to select the different categories and check the contribution list.

- **As a** developer, **I want to** see all directories, sorted by category, **in order to** better navigate through those files when needed.

- **As a** user of the dashboard, **I want to** be able to see general information about the repository, **in order** to easily get a general idea of the various topics without much effort.

- **As a** user, **I want to** see the content of a file **in order to** know what was changed.

- **As a** project manager, **I want to** be able to group files by the intensity of changes, **in order to** know which ones might need more attention or caution when merging, due to the multiple users committing to the same file.

- **As a** project developer, **I want to** see what files were changed when a commit is done **in order to** check if the changes can be merged to the main branch.

- **As a** project manager, **I want to** be able to choose which branch is going to be visualized, **in order to** keep track of what is being done by other team members before their work is merged to the main branch.

- **As a** user of the dashboard, **I want to** see the repository's file tree **in order to** have a graphical and easy way of seeing the repository's structure and be able to navigate through the multiple directories.

- **As a** user of the dashboard **I want** to be able to see general information about the repository, **in order to** easily get a general idea of the various repo properties without much effort.

Further analysis of this enumeration might lead to a higher number of requirements due to requisite subdivision, in order to simplify their design/implementation.

<small>
_Sancho Amaral Simões, 2019217590, 04/11/2021 - (v1.3)_
_André Carvalho, 2019216156, 17/10/2021 (v1.0)_
</small>
