# Readme of DES/Requirements designs

The designs for the implementation of the different requisites are in this directory. They are divided by the following categories:

-   General: general definitions/functionalities of the dashboard.
-   Repository: general information about the dashboard repo.
-   File: files in the dashboard repo.
-   Social: dashboard repo view as a team of contributors. 
-   Issue: GitLab issues of the dashboard repo.
-   Dynamic: functionalities common to each view that offer a dynamic perspective of the dashboard repo.
-   Other: other dashboard functionalities.
