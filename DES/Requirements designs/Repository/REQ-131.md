# REQ-131: Content of file on commit
## Phase 1: retrieve data from Gitlab server
- Make HTTP requests to the Gitlab server to gather the changed data of commits. The data can be obtained through the Repositories API compare method
## Phase 2: display info on the screen
- Display the data in an easy to read way for the user
## Acceptance Criteria

To be accepted, the implementation of this requirement must pass the following tests:
- Test with different repositories
- Test with different collaborator access or repository visibility levels
