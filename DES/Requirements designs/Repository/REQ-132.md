# REQ-132: Display repository files grouped by change intensity

## Phase 1: retrieve data from Gitlab server

-   Make a HTTP request to the Gitlab server to gather the list of commits. The data can be obtained through the use of the REST API resource Commits API.
-   Store the information needed in order to obtain the modification intensity per file.

 **GitLab REST API documentation: https://docs.gitlab.com/ee/api/api_resources.html

## Phase 2: calculate metrics and extract the results

-   Calculate the number of commits per file.
-   In order to quantify the intensity of modifications of a file define a criteria which considers time intervals and filter the commits associated to match the time interval defined.

## Phase 3: display info on the screen

-   Display the files grouped by modification intensity and in a pleasant view for the user.

## Acceptance criteria:

-   The files must be correctly grouped by modification intensity.
-   Different time intervals should not affect the correctness of the groups.
-   The implementation must be in compliance with the definition of the requirement.
