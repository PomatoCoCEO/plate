# REQ-191: File changes by commit

## Phase 1: Retrieve data from Gitlab server

- Make an Http request to the server to obtain the list of files of a certain commit via the CommitsAPI.

## Phase 2: Extract the results

- Filter the list by choosing the ones that were last commited.
- Different commits will show the modified files in order to verify if the changes can be merged to the main branch.
  If it is possible, the results will be extracted.

## Phase 3: Display the modified files there were last commited

- Display the list of files on the screen, making it easier to see the modified file that we want to use.

## Possible tests:

- Review the recent commits, obtaining the file that was modified.
