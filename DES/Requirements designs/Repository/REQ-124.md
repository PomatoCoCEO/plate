# REQ-124: Category View

## Phase 1: Retrieve data from Gitlab server

- Make HTTP requests for each view to collect the necessary data.
- The server queries the database to obtain the list of views.
- The data is returned to the user.

## Phase 2: Calculate metrics and extract the results

-  Obtain and distinguish each file category in order to capture as much information as possible.

## Phase 3: Display the views on the screen

- Organize the views by category in an easy way.

## Acceptance Criteria

To be accepted, the implementation of this requirement must pass the following tests:

- Create some views to check if it works and looks presentable.
- Try it with different user to see their opinions.
