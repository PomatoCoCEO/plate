# REQ-120: View commits' table of each active member

## Phase 1: Retrieve data from Gitlab server

- Make HTTP requests to the Gitlab server to obtain all commits made in the project

- Data can be obtained through the Commits API

## Phase 2: Organize the results

- With the data obtained, group it by member

## Phase 3: Display info on the screen

- Use the data to create a user-friendly display

- Allow multiple sort options

## Acceptance Criteria:

- The commits must be correctly listed for each user

- The implementation must be in compliance with the definition of the requirement
