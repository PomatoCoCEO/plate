# REQ-106: Commit dimensions

## Phase 1: Retrieve data from Gitlab server

- Make an Http request to the server to obtain the commit details

## Phase 2: Calculate metrics and extract the results

- Obtain the number of additions, deletions of the commit in the "stats" attribute

## Phase 3: Display the list of issues 

- Display the number of lines added and removed of the given commit

## Acceptance Criteria

To be accepted, the implementation of this requirement must pass the following tests:

- Select a past commit and verify if the displayed information is equal to the commit information
