# REQ-190: Select Branch

## Phase 1: retrieve data from Gitlab server

- Make HTTP requests to the Gitlab server to gather the desired branch.The data can be obtained through the Branches API Get single repository branch endpoint.

## Phase 2: display the data in an easy-to-read way for the user

- Display the branch data changes

## Phase 3: use of the results

- Use the the data to keep track of what is being done by other team members before their work is merged to the main branch.

## Acceptance Criteria:

- Create three different branches with different users and choose then visualize each of them.
