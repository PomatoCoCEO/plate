# *Plateboard* Requirements Testing Template

- This document has the purpose to provide a template for testing the implemented requirements.
- Every team member should follow this template for every test.

--- 

## Testing *Plateboard* Requirements
- The person testing a given requirement should specify all the tests done during the task, with the respective results.
- The tests must cover the acceptance criteria defined in the requirement's design
- The tester should be creative enough to add corner cases that may break the requirement and were not defined in the requirement's design. (example: try to access an invalid page of commits [-1])
- If there is a test with negative results, the tester should create an issue about this bug linking the respective requirement. This issue should be assigned to the person who implemented the requirement.

## Testing *Deishboard* Requirements
- The person testing a given requirement should specify all the tests done during the task, with the respective results.
- The tests must cover the topics defined in the requirement's specification (this information can be found in the deishboard release notes [GIT PATH: DEV/tst/Entrega_Deishboard_PL1.pdf])
- The tester should be creative enough to add corner cases that may break the requirement and were not defined in the release notes. (example: try to access an invalid page of commits [-1])

## Testing issue template example
### NOTE: The results should only be included in the issue's description after the tests were run
### Tests:
1. Open the repository list view and check if the list of private repositories is the same as the current repository list of the gitlab account.
2. Choose a repository and check if the repository summary page changes to the correct information about the new repository.
### Results:
1. ✔️ The private repository list is the same as the current repository list of the gitlab account.
2. ❌ After choosing a different repository, the repository summary page kept the information about the old one.
### DOD: The results were obtained for every tests defined in the previous lists.

<sub>*Samuel dos Santos Carinhas, 2019217199, 10/12/2021 - v1.0)*</sub>
