# _Plateboard_ repository conventions

This document has the purpose to explain to all the the members of this project the conventions adopted for its repository.

## Issues

- An issue name must be of the following format: **[ISSUE_TYPE]-[ISSUE_ID]: [ISSUE_TITLE]** (e.g.: _proc-2: Create repository conventions_). The issue title must not exceed the 72-characters limit. Further details of the issue may be present in its description.

- Two labels from two label sets must be associated to the created issue.
  The first label set:

  - **ARCH** - architecture.
  - **DEV** - development .
  - **EDU** - education.
  - **ENV** - environment.
  - **PM** - project management.
  - **QC** - quality control.
  - **PROC** - process and practice (documentation).
  - **PROD** - production.
  - **REQ** - requisites.
  - **TST** - testing.

- The second label set is:
  - **Improvement**.
  - **Bug**.
  - **Feature**.
  - **Task**.
- An estimate of the required time to complete the issue must be specified in its description (e.g.: /estimate 1d 2h 3m). This estimate must be a multiple of 20 minutes, using only _Fibbonacci numbers_ (1,2,3,5,8,13,...). Tasks longer than 4h20m (13x20m) should be divided in multiple smaller issues.
- A **DoD** (_definition of done_) is required in the issue description. Remember to always use the past tense.
- The assignee and the deadline must always be clear. If the deadline of the issue is exceeded, it may be changed if justified.
- The time spent on the issue must be periodically logged, usually after changing the current task or at the end of the day.
- To ensure a more informative development pipeline, change the **state label** of the issue in question whenever necessary.
  The allowed issue states are:
  - **To Do** - the issue is waiting to be worked on.
  - **In Progress** - the issue is being worked on.
  - **Frozen** - the issue development has stopped due to a justificable reason.
  - **In Review** - the issue work is being reviewed.
  - **Staging** - the issue work is being tested.
  - **Approved** - the issue has been approved.
  - **Done** - the issue is done and cannot fallback to any of the previous states.

Do not jump states.

## Reviewing issues

The QC team must always create issues while reviewing merge requests.
 
- The conventions to follow are the same as general issues (listed above). However, the author must choose 2 labels from the first category (ARCH, DEV, EDU, ...). The first label is QC, which is the ISSUE_TYPE, and the second one must be the one in the reviewing MRs. 
- The review issue must be linked to the one(s) reviewed.
- Each reviewer must only create one issue per topic (e.g.: Profiles, requirements, ...).

## Commits

- Before a local commit, stage all the wanted changes and review/test them carefully. After that, push the commit in order to be applied on the remote repo. DO NOT EVER commit changes that don't allow the correct execution of the application!
- Each commit must obey to the following format: **[ISSUE_TAG]-[ISSUE_ID]: [COMMIT_TITLE]**. The commit title must summarize the purpose of the changes applied. Do not write more than **72 characters** in the commit title. If you want to provide more details about the commit in question use the commit **description** field.

## Branches

- The repository must have a **core branch** (e.g. main). This branch is the **most stable** one and used for production. From it may derive other branches that correspond to a specific client in order to attend to their disctinct requisites (e.g. _main, main-[client1], main-[client], ..., main[clientN]_). Only the repo maintainers are allowed to push to these branches.
- The repository must have a **staging** branch that allows the quality control enforcers/reviewers to test the application. Several other testing branches may be created for each client. These branches must follow the format **dev-[ADITIONAL_INFO]**.
- For each issue that is considered to affect significantly other collaborators' work it is mandatory the creation of a new branch with a name that obeys to the following format: **[ISSUE_TYPE]/[ISSUE_TAG_UPPERCASE]-[ISSUE_ID]-[DESCRIPTION]**. The branch description must not exceed the **72-characters** limit and each of its words must be in lowercase (with the exception of the ISSUE\_TAG) and separated by a hyphen. An example of a valid branch name is _improvement/IMP-32-add-virtual-scroll-to-grids_.
- To create a branch associated with a given issue always select the option "Create branch" instead of "Create merge request".
- Small fixes that are guaranteed to not destabilize the previously stable branches might be directly commited on the stable branches - **hot fixes**. If you are not an maintainer of this repo, you shall request one of the admins to perform this operation.
- Update your working branch as much as possible, by merging the core branch.
- The repository must have branches folders, named after the used issue type labels and other wanted criteria (e.g.: bug, improvement, task, feature, staging, stable, ...). An example of this structure might be:

  - origin
    - bug
      - IMP-32: ...
      - IMP-10: ...
        ...
    - improvement
      - IMP-5: ...
      - IMP-2: ...
        ...
    - task
      - REQ-8: ...
      - ARCH-1: ...
        ...
    - feature
      - REQ-7: ...
      - ARCH-3: ...
        ...
    - stable
      - stable-[client1]
        ...
      - stable-[clientN]
    - staging
      - dev
      - dev-[client1]
        ...
      - dev-[clientN]

**Note**: a branch folder is automatically created whenever you create a branch that obeys to the format [FOLDER_NAME]/[BRANCH_NAME].

## Merge requests
- A merge request must only be created when the developed task is ready to be reviewed.
- A merge request description must obey to the template specified in the file _PROC_MergeRequestDescriptionTemplate.md_.
- A merge request must have at least two different reviewers (maintainers or not) with one of them being the assignee (a repo maintainer) in order to be approved. The assignee will perform the merge to the stable(s) branch(es).
- For a better repository history maintenance, make sure that the options "_Delete source branch when merge request is accepted._" and "_Squash commits when merge request is accepted._" are **deselected**.
- All the suggestions coming after the review must be specified in the form of **threads**.
- Each thread must be solved by the contributor and might also be solved  by the reviewer(s), in exceptional cases.

<sub>_Sancho Amaral Simões, 2019217590, 06/10/2021 - (v1.3)_
_Review: André Carvalho, 2019216156, 07/10/2021 (v1.1)_
_Update: André Carvalho, 2019216156, 27/10/2021 (v1.2)_ </sub>
