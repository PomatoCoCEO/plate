# *Plateboard* MR description template  

- This document has the purpose to specify to all the the members of this project the used merge request description template.
- The symbols // and {} are used to display comments/tips/placeholders about this template.   
- Note that if a region delimited by its title has no text inside you may remove it from your MR description.  

---  

# {BRANCH_NAME} - Merge request

## Issues dependencies

// Specify the issue(s) associated with this merge request. Note that the work done in this branch may be associated with several registered issues.

* `{ISSUE_1}` - {ISSUE_1_LINK}  
...
* `{ISSUE_N}` - {ISSUE_N_LINK} 

## MR relational dependencies

// Specify the MRs that are related to the current MR.

* `{MR_1}` - {MR_1_LINK}  
...
* `{MR_N}` - {MR_N_LINK}  


## MR blocking dependencies

// Specify the MRs that block the approval of the current MR.

* `{MR_1}` - {MR_1_LINK}  
...
* `{MR_N}` - {MR_N_LINK}  

## What was implemented

// Enumerate the goals implemented in this branch (past simple).

* {GOAL_1}  
...
* {GOAL_N}

## How was implemented

// Specify the design pattern applied, the business logic and the special cases to consider (past simple).


* {DES_PATTERN_1}  
...
* {DES_PATTERN_N}
* {LOGIC_1}  
...
* {LOGIC_N}
* {SPEC_CASE_1}  
...
* {SPEC_CASE_N}

## New endpoints

// Enumerate the new endpoints (if created) and their purpose (present simple).

- `{ENDPOINT_1}` - {ENDPOINT_1_OPERATION}  
...
- `{ENDPOINT_N}` - - {ENDPOINT_N_OPERATION}

## Updates to the dev environment

// Explain wich updates were applied to the project that imply the execution of special tasks somewhat external to the project (e.g. **sudo apt install {package_name}**, run a given script, etc.; present simple/imperative).
- {TASK_1}.  
...  
- {TASK_N}

## How to test

// Explain in a sequential way how to properly test the work done in this branch.
- 1 - {STEP_1}.  
...  
- N - {STEP_N}

--- 
<sub>*Sancho Amaral Simões, 2019217590, 06/10/2021 - v1.0)*</sub>
