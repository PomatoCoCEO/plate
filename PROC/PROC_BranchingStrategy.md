# Branching Strategy

- The following document serves to describe the Trunk Based branching strategy. This strategy should be followed by team members whenever they want to add a new feature to the existing code.

## Trunk Based Description

- There is a main and stable branch with all the development features called **development**. This branch includes all the backend and frontend code;
- When a member wants to implement a new feature and include it in the **development** branch, it must create a new branch from **development** to build it;
- The new branches created should have a short lifetime in order to be easily included in the source branch without any conflicts;
- When the feature is complete, a merge request must be created to integrate it into the **development** branch according to the conventions already established;

## Deployment

- The deployment is executed from the **main** branch. Each time a requirement has been completed, it is stable and has passed all tests in the **development** branch it can be merged into the **main** to be deployed;

## Exceptions

- The documentation can be directly merged from the branch where it was created into the **main** branch since the changes they make to the repository do not affect code development;
- In case of urgency and if it is only to change small coding bugs or typos, commits can be made directly to the **development** branch, but the cause must be justified in the commit message. 
  
---
    <sub>*Joana Simões, 2019217013, 27/11/2021 - (v2.0)*</sub>
