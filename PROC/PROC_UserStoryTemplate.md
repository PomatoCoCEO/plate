# **Plateboard** user story template

All user stories should contain the following elements in order to be approved:

-   **User**: it should be clear *who* the feature to implement is meant to benefit (e.g. project member, backend developer), Try to be specific.
-   **Feature**: a brief and clear *feature description* should be included, to specify what to implement.
-   **Value/Purpose**: the user story should clarify what is the *purpose* of the feature to implement (e.g. provide security, allow for scrutiny, monitoring work). This makes the feature *negotiable*: a simpler feature could be implemented instead of the proposed one, with the client's approval, as long as it helps reach the desired goal.
-   **DOD**: Definition of done: a *clear* statement of what it means to have concluded the implementation of the user story.

It is recommended that *tests* are devised and described, to guide quality control members in the process of validating feature implementations.

An example of a template-abiding user story is the one defined in REQ-106 or REQ-107. Even though the structures are different, all the compulsory elements are included in both.

<sub>Paulo Cortesão, nº2019216517, v1.0, 11/10/2021<sub>