## PROC: Test Cases
This document contains the steps on how to create tests to use in the pytest framework. Each team member that is designated to create tests should follow this document.
## 1) Installation
The first step is to install pytest. Before merging the test cases team members should verify if their tests are working in their device. In order to install you should  follow this process:

**Step 1)** You can install pytest by

    pip install pytest
    
**Step 2)** Verify the installation by 

    py.test -h
    
## 2) Test File Designation
In order to create tests they need to be located in files whose names begin with :

    test_

or end with:

    _test.py
 
Moreover, in the beggining of the file it has to be:

    import pytest

## 3) Test File Functions
Now that the test file has been created and properly designated, the team member is able to start creating the functions that will operate in the test. For each function that evaluates a case the team member wants to test/check, it also has to be created an assert (bool that returns True or False) function that begins with test_. This is important to ensure that the function does what it is meant to do. For example if we have this file test: 
```python
# test_addOne.py
import pytest
def addOne(x):
    return x+1
```
it must be added the function:
```python
# test_addOne.py
import pytest
def addOne(x):
    return x+1

def test_addOne():
    assert addOne(5) == 6
```
run (it will run all the tests):    
    
    $ pytest 
or run (it will run this specific test):

    $ pytest test_addOne.py       

Output would be:

     test_addOne.py .  

## 4) How to use Pytest Fixtures
Used in case a team member wants to run some code before every test method . In addition, instead of repeating the same code in every test we define fixtures to de-duplicate the code. To use this it is needed to put `@pytest.fixture` before a function. For example:
```python
#test_fixturesExample.py
import pytest
@pytest.fixture
def array():
    first=5
    second =15
    third=25
    return [first,second,third]

def test_comparewithFirst(array):
    num=15
    assert array[0]==num,"comparison failed"

def test_comparewithSecond(array):
    num=15
    assert array[1]==num,"comparison failed"

def test_comparewithThird(array):
    num=15
    assert array[2]==num,"comparison failed"

```
As a result of this program, only the python test_comparewithSecond will pass.
Output would be:

    test_fixturesExample.py F.F  

  

## 5) How to Parametrize Test Functions

 The aim of parameterizing a test is to run it against multiple sets of arguments. We can do this by using ``@pytest.mark.parametrize.``
 For example: 
 ```python
 #test_case1
import pytest
@pytest.mark.parametrize("varX, varY, varZ",[(2,5,7),(4,6,15)])
def test_add(varX, varY, varZ):
    assert varX+varY == varZ,"failed"
```
Output would be:

    test_case1.py .F  
    =============================FAILURES=============================  
    varX = 4, varY = 6, varZ = 15
    (...)
    @pytest.mark.parametrize("varX, varY, varZ",[(2,5,7),(4,6,15)])
    def test_add(varX, varY, varZ):
    >       assert varX+varY == varZ,"failed"
    E    AssertionError: failed
    E    assert (4 + 6) == 15
This is efficient way to test different set of values without having a lot of repeated code, covering multiple cases.

## 6) How to skip/xfail a test

In order to skip a test the team member needs to write `` @pytest.mark.skip `` before the assert funtion. The test won't be executed.
In order to xfail a test the team member needs to write `` @pytest.mark.xfail `` before the assert funtion. The test will be executed but it won't count to the passed and fail tests.

 For example:
```python
#test_fixturesExample.py
import pytest
@pytest.fixture
def array():
    first=5
    second =15
    third=25
    return [first,second,third]
    
@pytest.mark.xfail
def test_comparewithFirst(array):
    num=15
    assert array[0]==num,"comparison failed"

def test_comparewithSecond(array):
    num=15
    assert array[1]==num,"comparison failed"
@pytest.mark.skip
def test_comparewithThird(array):
    num=15
    assert array[2]==num,"comparison failed"
```
Output would be:

    test_fixturesExample.py x.s  


*Gonçalo Vasconcelos Correia 2019216331 1/12/2021*

