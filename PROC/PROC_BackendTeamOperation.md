# PROC: Backend Team Operation

This document contains an overview of the data structure classes as well as a distribution of tasks related to its creation. Each member of the backend team must implement a class or create the tests that were assigned.

---

## Class Repository

**Responsible**: Joana Simões

**Repository Attributes:**

    - File list
    - Member list
    - Branch list
    - Issue list
    - Commit list

**Repository Functions:**

    - Get active members
    - Get total time by category

## Class File

**Responsible**: Sílvio Saldanha

**File Attributes:**

    - Name
    - Commit List
    - Category
    - Issue list
    - Dimension

**File Functions:**

    - Get Author List
    - Get the number of changed lines
    - File directory
    - Get pending defects
    - Get stability metrics
    - Get code complexity

## Class Member

**Responsible**: João Botelho

**Member Attributes:**

    - Username
    - Name
    - Email
    - Issue List
    - Avatar URL
    - Commit List
    - Biography
    - GitLab role

**Member functions:**

    - Number of commits
    - Get Files changed by this member

## Class Branch

**Responsible**: Guilherme Almeida

**Branch Attributes:**

    - Name
    - Merged (true or false)
    - Protected (true or false)
    - Default (true if its the main branch)
    - Commit list (get the commit list (if ref_name is undefined, it means its the main branch))

## Class Issue

**Responsible**: Samuel Carinhas

**Issue Attributes:**

    - ID
    - State
    - Description
    - Title
    - Author
    - Milestone
    - Assignee List
    - Label list
    - Due date
    - Estimated time
    - Time spent
    - Creation Date
    - Closed Date (may be null)
    - URL

**Issue functions:**

    - Get related merge requests
    - Get comments
    - Get related issues

## Class Commit

**Responsible**: João Artur

**Commit Attributes:**

    - ID
    - Title
    - Author
    - Message
    - Committed Date

**Commit functions:**

    - Get modified files
    - Get added files
    - Get deleted files

## Test cases

**Responsible:**

    - Vicente Tiago
    - Gonçalo Correia
    - Telmo Lopes

---

    <sub>*Joana Simões, 2019217013, 02/11/2021 - (v1.0)*</sub>
