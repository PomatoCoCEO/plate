# **Plateboard** solution design template

A solution design should be contained in a MD file named *REQ-<ID of the requirement>*. In order for be approved, each solution should obey the following approach:

## Phase 1: Give insights on where the data needed might be obtain and explain how to do it.
	-   Here should be listed the steps required to obtain the data and the method/protocols used in each one.

## Phase 2: Make clear which results should be extracted from the data collected.
	-   List the metrics/results to be extracted in order to implement the requirement.

## Phase 3: Explain what to do with the results.
	-   Give instructions on what should be done with the results obtained in the previous phase.

## Acceptance criteria: Define incisive and concrete acceptance criteria.
	-   List the criteria which the implementation will need to pass in order to be considered accepted. Beware that the design and the acceptance criteria must be in compliance with the requirement.

**Notes:** All of the steps/instructions should be clear and concise as to avoid any misunderstading that could cause further delays on the project development. Any links, documentation or tutorials relevant to any of the phases listed bellow should be associated to the respective stage of the solution.

An example of a solution design that abides by this template is REQ-118.md.

<sub>João Artur, 2019217853, v1.0, 16/10/2021<sub>
