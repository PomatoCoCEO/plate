# ROLES

## Responsibilities of each role

### Project Manager

    - Manage project;
    - Interact with team members;
    - Preside meetings

### Secretary

    - Write minutes of meetings;
    - Organize documentation;

### QC(Source control and quality control)

    - Enforce conventions;
    - Review and test code, providing feedback to the authors;
    - Review issues from all categories except REQ;

### AQ (Architecture)

    - Plan the implementation of the user stories;

### REQ

    - Review user-stories;
    - Communicate with the Client and with the developers;

### BE (Backend)

    - Program the interaction with the database;
    - Create the API;
    - Document code;

### FE (Frontend)

    - Plan and program the views' layout
    - Collaborate with the BE in endpoint definition
