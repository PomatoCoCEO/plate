# PROC : Release Notes

## Implemented Features 
 
 **REQ-123 : Choose GitLab repository**

- **User Story :** As an user of the dashboard I need to choose the repository I want to access, whether it is a public or a private one, in order to only get the information I'm looking for. 
 
- **Design :** 

	 **Phase 1 : Retrieve data from Gitlab server**
	 - Make HTTP requests to the Gitlab server to gather the list of public Gitlab repositories and the private ones the user has access to. 
 
	 **Phase 2 : Display the list retrieved on the screen** 
	 - Display the repositories to the user, filtering public and private.
 
  **Phase 3 : Choose the repository from the list**
  - Select which repository the user wants to see from the list. 
  																														
  **Acceptance Criteria :** 
	  To be accepted, the implementation of this requirement must pass the following tests:
	- Select a repository and check whether you can see its information. 
	- Test with different users, with different private repositories. 
  
- **Link :**  [http://10.17.0.240:8080/repositories/](http://127.0.0.1:8000/repositories/ "http://127.0.0.1:8000/repositories/") (click on "Repositories" and then "Search for private[/public] repositories")
  
 **REQ-130 : Add page that summarizes repo information** 
 
  - **User Story :** As a user of the dashboard I want to be able to see general information about the repository, in order to easily get a general idea of the various repo properties without much effort. 
  
  - **Design :** 
	
	**Phase 1 : Retrieve data from Gitlab server** 
	- Make several HTTP requests to the Gitlab server to gather all the data required. The data can be obtained through the use of several REST API resources such as Projects API (number of commits, size of repository, number of forks, number of stars, programming language distribution), Repositories API (number of collaborators, average commits per collaborator), Commits API (commit activity), Branches API (number of branches) and Issues Statistics API (number of closed issues). - After each HTTP request, store the information needed in order to use it later GitLab REST API documentation: [https://docs.gitlab.com/ee/api/api_resources.html](https://docs.gitlab.com/ee/api/api_resources.html) 
	  
	 **Phase 2 : Calculate metrics and extract the results**
	 - Obtain the project manager.
	 - Obtain number of collaborators and other important metrics through a direct method or calculations. 
	   
	 **Phase 3 : Display info on the screen**
	 - Arrange the data in an appealing fashion for the user.
	   
	**Acceptance Criteria :** 
	To be accepted, the implementation of this requirement must pass the following tests: 
	- Test with different repositories. 
	- Test with different collaborator access or repository visibility levels.

- **Link :** http://10.17.0.240:8080/ (clicking on the logo will redirect to this view)
   
**REQ-108 : Total Effort** 

- **User Story :** As an user I want to see the total effort applied in each category in order to have an idea of the priority distribution. 
 
- **Design :** 

	**Phase 1 : Retrieve data from Gitlab server**
	- Make HTTP requests to the Gitlab server for each category to gather of data necessary. 
	- The data can be obtained through the Repositories API compare method. 

	**Phase 2 :  Calculate metrics and compare the results** 
	- Obtain the sum of the time spent for each category. 
	- Order categories based on time spent. 

	**Phase 3 :  Display info on the screen**
	- Arrange the data in an appealing fashion for the user.
	 
	**Acceptance Criteria :** 
	To be accepted, the implementation of this requirement must pass the following tests: 
	- Test with different repositories 
	- Test with different collaborator access or repository visibility levels 

- **Link :** [http://10.17.0.240:8080/team_page/](http://127.0.0.1:8000/team_page/ "http://127.0.0.1:8000/team_page/") (click on "Members" and then on "Check total effort")

**REQ-185 : Repository's file tree** 

- **User Story :** As a user of the dashboard, I want to see the repository's file tree in order to have a graphical and easy way of seeing the repository's structure and be able to navigate through the multiple directories, by clicking in each one. 

- **Design :** 

	**Phase 1: Retrieve data from GitLab server**

	- In the case of trying to access a private repository, first, it's necessary to use REQ-107 to get an authentication token.
	- It's necessary to also use REQ-123 to choose the GitLab repository
	- Make an HTTP request to the GitLab server to acquire the content of the initial directory. The response will be a list and the "type" attribute of each element will distinguish between directories and files.
	- When the type is "tree" (a directory), it's necessary to make a new HTTP request as described above additionally with the attribute "path" of that directory ("path" parameter of the element) to also get the content of that directory.

	GitLab API repository documentation: <https://docs.gitlab.com/ee/api/repositories.html> and <https://docs.gitlab.com/ee/api/repository_files.html>
	
	**Phase 2: Extract the results**

	- Create a list of files/directories in the current directory


	**Phase 3: Display information on the screen**

	- Display the data in a table format
	- First, only display the content of the initial directory (project directory)
	- By clicking on a directory, its content is displayed in a new page
	- The operation for files is similar, when clicking on them, their content must be open in a new page
	- In the table, the files should also contain their last commit
	Note: Directories and files can be easily distinguished by the last commit field.

	**Acceptance Criteria**
	
	To be accepted, the implementation of this requirement must pass the following criteria:

	- The files and directories of that branch match the ones in the gitlab website.
	- Selecting a directory opens a new page containing its files/directories.
	- The last commit dates of a file are correct, according to the gitlab website.
	- By clicking in a file, its details page is opened. 




- **Link :** http://10.17.0.240:8080/files/ (click on "Files")

**REQ-118: View all commits of a file and who made them**

- **User story**: As a project member, I want to see the list containing all the commits of a given file in order to know who changed it at any given time.

- **Design**: 

	**Phase 1: retrieve data from Gitlab server**

	-   Make an HTTP request to the Gitlab server to obtain the list of commits associated with a file, including its authors.
	-   After obtaining the list, query the server on the relevant diffs.
	-   (this could be achieved by saving the data on the repo in a DB local to the server and then querying).

	**Phase 2: calculate metrics**

	-   Obtain the percentage of modified lines and other important metrics

	**Phase 3: display info on the screen**

	-   Arrange the data in an appealing fashion for the user

	**Acceptance Criteria**
	To be accepted, the implementation of this requirement must pass the following tests:
	-   Test with invalid files
	-   Test with files whose name was altered

- **Link**:  [http://10.17.0.240:8080/files/](http://127.0.0.1:8000/files/ "http://127.0.0.1:8000/files/") and then click on a file

Writing: André Marques, Tomás Ferreira and Vicente Tiago
Reviewing: André Carvalho, Gonçalo V. Correia and Paulo Cortesão 

