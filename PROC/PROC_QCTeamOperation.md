# PROC: Quality Control Team Operation

This document contains the distibution of tasks for the QC team. 
When creating a merge request, each member should assign it to the correspondent maintainer and put one of the members of the QC team as a reviewer.

---

## Distribution of QC Team Members

|Members            |Assignee (Maintainer)  |Reviewer (QC)      |
|-------------------|-----------------------|-------------------|
|André Carvalho     |Sancho Simões          |André Marques      |
|André Marques      |André Carvalho         |Gonçalo V. Correia |
|Gonçalo Barroso    |André Carvalho         |André Marques      |
|Gonçalo Correia    |André Carvalho         |André Marques      |
|Gonçalo V. Correia |André Carvalho         |André Marques      |
|Guilherme Almeida  |André Carvalho         |André Marques      |
|Joana Simões       |André Carvalho         |Gonçalo V. Correia |
|João Artur         |André Carvalho         |Gonçalo V. Correia |
|João Botelho       |André Carvalho         |Gonçalo V. Correia |
|João Garcia        |André Carvalho         |Gonçalo V. Correia |
|João Geirinhas     |André Carvalho         |Gonçalo V. Correia |
|João Guilherme     |Sancho Simões          |João Geirinhas     |
|João Marques       |Sancho Simões          |João Geirinhas     |
|Paulo Cortesão     |Sancho Simões          |João Geirinhas     |
|Rui Marques        |Sancho Simões          |João Geirinhas     |
|Samuel Carinhas    |Sancho Simões          |João Geirinhas     |
|Sancho Simões      |André Carvalho         |João Geirinhas     |
|Sílvio Saldanha    |Sancho Simões          |João Marques       |
|Sofia Costa        |Sancho Simões          |João Marques       |
|Telmo Lopes        |Sancho Simões          |João Marques       |
|Tomás Ferreira     |Sancho Simões          |João Marques       |
|Vicente Tiago      |Sancho Simões          |João Marques       |

---
<sub>*Sofia Torres Costa, 2019209981, 18/10/2021 - (v1.0)*</sub>  
