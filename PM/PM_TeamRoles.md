# Team Roles
## Roles assigned to each team member

### Project Management

    - Paulo Cortesão - Project Manager
    - André Carvalho - Assistant Project Manager

### Secretaries

    - Joana Simões
    - João Artur

### SC (Source Control) and QC (Quality Control)

    - André Figo - Team Leader
    - Sancho Simões
    - João Geirinhas
    - André Marques
    - João Marques
    - Gonçalo V. Correia

### ARCH

    - Samuel Carinhas
    - João Artur
    - Paulo Cortesão

### REQ

    - André Figo - Team Leader
    - Sancho Simões
    - João Geirinhas
    - Vicente Tiago

### BE (Backend)

    - Samuel Carinhas - Team Leader
    - Joana Simões
    - Guilherme Almeida
    - João Botelho
    - João Artur
    - Vicente Tiago
    - Gonçalo Correia
    - Sílvio Saldanha
    - Telmo Lopes

### FE (Frontend)

    - Sofia Costa - Team Leader
    - Sancho Simões
    - João Guilherme
    - João Garcia
    - Gonçalo Barroso
    - Tomás Ferreira
    - Rui Marques
