# João Garcia
**Gitlab ID**: @joaodavidgarcia2001

**Student Number:** 2019216375

![img](./T023W2Y1C9L-U02409HD9QV-5665d68a7d76-512.png)

- 19 years old (20/12/2001)
- Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

-  **Hobbies:**
  - Gym
  - WaterPolo player

- **Main Roles:**
  -Frontend Developer

-**Activities Conducted:**
  - Learn Git & Gitlab (#21)
  - Learn Django Basics (#59)

  - PROC related tasks:
    - Create profile folder (#98)
  
  - User stories (US):
    - Distinguish each file category (#124)

  - Profile:
    - Update member profile (#406)

  - Requirements' designs :
    - Design solution for REQ-125 (#153)
    - Design solution for dynamic view (#314)

  - Design:
    - Design footer and purple (#262)

  - Quality control:
    - Review REQ-123 tested implementation and checked for errors (#361)
