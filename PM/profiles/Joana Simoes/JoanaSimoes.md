# Joana Simões

## @PugtgYosuky

![img](Joana_Simoes_photo.jpg)

- 20 years old
- Attending the 3rd year of the Bachelor's degree in Informatics Engineering at the University of Coimbra
- Student number: 2019217013

### Hobbies

- Photography
- Long walks in nature
- Draw

### Contacts

- DEI email: joanasimoes@student.dei.uc.pt
- UC email: uc2019217013@student.uc.pt

### Roles

- Secretary
- Backend Developer

### Activities Conducted in the Project

#### PM

##### Project Management

- Write the minutes (#27, #79, #133, #167, #180, #221, #248, #268, #306, #329, #376)
- Team roles document creation (#135)

##### Meetings

- Backend meetings (#205, #282)

##### Profile

- Create user profile document (#90)
- Update profile with the activities done during the semester (#370)

#### PROC

- Minutes template creation (#35)
- Branching strategy document creation (#275 and #313)
- Backend team operation document (#214)

#### EDU

- Learn Git and GitLab (#13)
- Learn LaTex (#27)
- Learn Django basics (#37, #91)

#### DEV

- Backend work:
  - Class label (#207)
  - Class Repository functions: __load_info__, __parse__, __load_labels__, update_labels, __list_active_members, get_time_values, get_issues_statistics, get_number..., get_languages (#207)
  - Refactor code (#379)
- Frontend work: REQ-130 implementation (#285)

#### ENV

- Add requests dependency to CI (#379)

##### REQ

- REQ-118 user-story creation (#118)

#### DES

- REQ-107 design solution (#139)
- REQ-185 design solution (#194)

#### TEST

- Test DEIshboard requirement (#391)
