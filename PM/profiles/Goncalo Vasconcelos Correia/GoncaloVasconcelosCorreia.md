# Goncalo Vasconcelos Correia
**Gitlab ID**: @goncalovasconceloscorreia

**Student Number:** 2019216331

![img](./ProfileGVCorreia.jpg)

- 20 years old (19/01/2001)
- Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

- **Hobbies:**
  - Tennis/Padel
  - Video games

- **Main Roles:**
  - Quality Control Member

- ### **Activities Conducted:**
  - **EDU**
    - Learning or reviewing technologies:
      - Learn Git & Gitlab (#8)
      - Learn Django Basics (#39)
      - Python review(#40)

  - **PROC**:
    - Test cases document (#335)

  - **REQ**
    - User stories (US):
      - List of active/open issues at a given time (#116)
      - Select Branch (#190)
  - **DES**
    - Requirements' designs :
      - Design solution for REQ-113 (#144)
  - **PM**
    - Meetings:
      - Quality control team meeting (#239) 
      - Weekly team meeting out of class hours (#222, #223)
  
    - Profile:
      - Create profile folder (#85)
      - Update member profile (#357)
  - **ENV**
    - Pipeline:
      - Beggining of the project pipeline during a meeting (#239)
   
  - **DEV**:
    - Implement REQ-298 - List of all directories by category (#203)
  - **TEST**
    - Requirement testing
      - Test DEIshboard's REQ - Loading Page (#392)

  - **QC**: 
    - Minutes of meeting reviewing (#196,#234,#294,#295,#319,#333,#380)
    - PROC reviewing (#297)
    - Review and approve merge requests about about requirments' designs assigned to me (#164)
    - Review FE team css classes (#296)
    - Review and correct Gitlab issues according to the teacher's feedback (#323)
    - Review HTML base template page (#303)
    - Review Release notes and PDF document to submit in Inforestudante (#369)
    - Review requirements implementation MRs (#356)
    - Review mockup designs MRs (#341)
    - Review Open Issues Estimate Time(#348)
    - Review Requiremens' updates(#400)
    - Review the code rating optmization merge request(#402)
    - Review divide role document into two(#418)
    -  Review Release notes 2.0(#429)
    
  


