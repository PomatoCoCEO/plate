# Sancho Amaral Simões

![img](./Sancho_Simões.jpg)

-   20 years old
-   Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

-   Hobbies:

    -   Piano
    -   Trombone
    -   Basketball
    - 	Gymnasium workout
    -   Gaming

-   Roles:
    -   Source control manager
    -   Frontend Developer
