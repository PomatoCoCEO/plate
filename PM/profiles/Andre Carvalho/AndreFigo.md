# André Figo Carvalho

![img](./Andre_Figo.jpg)

- 19 years old (21/11/2001)
- Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

- Likes:

  - Sports
  - Video games
  - Tech

- ### Main roles:
  - Requirements team manager
  - Quality control team manager
  - Repository maintainer
  - Assistant project manager    

- ### Activities Conducted in the Project:
  - EDU:
    - Learning or reviewing technologies:
      - Learn Git & Gitlab (#53)
      - Learn Basics of Django (#19)
      - Review python and PostgreSQL (#54)

  - PROC:
    - Requirement implementation status document (#310)
    - Update requirement implementation status document (#311)
    - Update repository conventions (#150, #186)
  
  - REQ:
    - User stories (US):
      - Commit dimensions US (#106)
      - Repository's file tree (#185)
      - Updating a US based on implementation feedback (#346)
    
    -  Requirements' Mockups:
        - Design repository views (#286)
        - Add mockup view that was deleted by mistake (#421)
        - Updating a mockup based on implementation feedback (#346)
    
    -  Requirements' structuring:
        - Structure and prioritize requirements (#173)
        - Review first requirements US (#142)

  - DES:
    -  Requirements' designs :
        - Design for REQ-123 (#141)
        - Updating a design based on implementation feedback (#346)

  - PM:
    - Meetings:
      - Requirements team meeting (#230 )
      - Quality control team meeting (#241) 
      - Weekly team meeting out of class hours (#212, #213)
      - Managers' meeting (#308)
    
    - Project management:
      - Plan FE team tasks with FE team manager (#277)
      - Distribute testing issues among team members (#383)
      - Task distribution for the week (#345)
      - Prepare final dashboard presentation (#417)
      - Lead team the meetings in PM's absence (twice)
    
    - Profile:
      - Create user profile document (#86)
      - Update user profile with activities done during the project (#393)
    
  - ENV:
    - Begging of the project pipeline during a meeting (#241)
    - CI pipeline implementation continuation (#260)

  - DEV:
    - Implement REQ-118 (#203)
    - Implement REQ-112 (#419)
    - Implement REQ-131 (#419)
    - Restructured source .html/.py files in the project, with some bug correction (#359)

  - TEST:
    - Requirement testing
      - Test DEIshboard's REQ - Authenticated users with exclusive access (#395)

  - QC:
    - Meeting of minutes reviewing (#192, #233, #259, #276, #312, #334, #384, and others with no issue)
    - PROC reviewing (#32, #219, #290, #324, #394, #344)
    - Review and accept profile merge requests (#114)
    - Review and accept merge requests about first requirments' designs (#157)
    - Review project mockup and FE team css classes (#263)
    - Review and correct Gitlab issues according to the teacher's feedback (#321)
    - Review diagram that summarizes requirements structure (#184)
    - Review Release notes and document to submit in Inforestudante (#368)
    - Review requirements implementation MRs (#343)
    - Review mockup designs MRs (#289)
    - Verify Repository Structure (#3)
    - Verify that everyone was added to the repository (#30)
    - Review code refractoring and design update (#414)
    - Review Release notes 2.0 (#427)
  


  
  
