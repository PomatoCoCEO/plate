#João Marques

**Gitlab ID:** jm.grvm

![img](./Profile_Pic.jpg)

21 years old (01/03/2000)
Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra. Student Number: 2018278463


**Hobbies:**

Basketball, F1

**Roles:**

Quality Control

**Activities Conducted:**

Learning or reviewing technologies:

- Learn Git & GitLab(#60)
- Learn Django basics(#61)
- Review python(#62)

PROC related tasks:

- Create profile folder(#100)
- Release Notes document(#420)

User stories (US):

- Applied Effort Evolution(#110)

Requirements' designs :

- Design solution for REQ-112(#156)

Development:

- Implement REQ-120 - View commits' table of each active member(#328)

Meetings:

- Quality control team meeting (07/11/2021)(#240)

Quality control:

- Review and correction of expired issues(#320)
- Review Update Branching Strategy(#322)
- Test REQ-185(#364)
- Test REQ-108(#374)
- Test REQ-131(#423)
