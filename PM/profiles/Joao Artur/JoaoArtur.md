# João Artur

![img](./Joao_Artur.jpg)

- 20 years old
- Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

- Hobbies:
    - Scouts

- Roles:
    - Secretary
    - Backend Developer
    - Dashboard Architect

- Activities Conducted in the Project:
  
    - Learning or reviewing technologies:
        - Learn Git & GitLab (#7)
        - Learn LaTex (#34)
        - Learn Django basics (#42)
        - Review Python (#44)
        - Review PostgreSQL (#46)

    - PROC related tasks:
        - Create solution design template (#137)

    - User stories (US):
        - Commit's timeline and modified files per member (#119)
        - View uniform timeline (#177)
        - List of active members (#181)

    - Requirements' designs :
        - Design for REQ-130 (#138)
        - Design for REQ-132 (#178)

    - Requirements' updates:
        - Update user story and design for REQ 130 (#399)

    - Meetings:
        - Backend meeting (#201)
        - Backend meeting 2 (#281)

    - Project management:
        - Write Minutes of Meeting N1 (no issue associated)
        - Write Minutes of Meeting N2 (#26)
        - Update Minutes of Meeting N1 to LaTeX (#36)
        - Write Minutes of Meeting N3 (#78)
        - Write Minutes of Meeting N5 (#168)
        - Write Minutes of Meeting N6 (#179)
        - Write Minutes of Meeting N7 (#220)
        - Write Minutes of Meeting N8 (#249)
        - Write Minutes of Meeting N9 (#269)
        - Write Minutes of Meeting N10 (#307)
        - Write Minutes of Meeting N11 (#330)
        - Write Minutes of Meeting N12 (#377)


    - Profile:
        - Create profile folder (#84)
        - Update member profile (#398)

    - Architecture:
        - Create architecture document (#175)

    - Development:
        - Implement REQ 185 (#352)

    - Requirement testing:
        - Test DEIshboard's REQ - View files by category (#397)
