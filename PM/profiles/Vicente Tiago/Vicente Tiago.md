# Vicente Tiago

![img](./VicenteTiagopicture.jpg)

-   21 years old
-   Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

-   Hobbies:

    -   Playing some videogames
    -   practise sports

-   Roles:
    -   Requirements team
    -   Backend Developer

- **Activities Conducted:**

  - Learning or reviewing technologies:
    - Learn Git & Gitlab (#18)
    - Learn Django Basics (#48)
    - Python review(#102)
    - PostgreSQL(#47)

  - PROC related tasks:
    - Relesase notes document(#338)
  
  - User stories (US):
    - Navigate between views(#113)
    - File changes by commit(#191)
    - Participation in the requirements team(#171, #231)
   
  -  Requirements' designs :
       - Design solution for REQ-116(#149)

  - Meetings:
    - Team weekly reunion (25/10/2021)(#229)
    - Team weekly reunion (01/11/2021)(#228) 
    - Requirements team meeting (04/11/2021)(#231)
    - Backend team reunion (20/11/2021)(#280)
  
  - Profile:
    - Create profile folder (#71)
    - Update member profile (#403)
   
  - Development:
    - Implement REQ-181- List of active members(#375)
