# Silvio Saldanha
**Gitlab ID**: @silviosaldanha

**Student Number:** 2016219177

![img](./profile_picture.jpg)

- 23 years old (06/09/1998)
- Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

-  **Hobbies:**
  - Gaming
  - Sports

- **Main Roles:**
  -Backend Developer

-**Activities Conducted:**
  - Learn Git & Gitlab (#11)
  - Learn Django Basics and review Python (#49)
  - Review PostgresSQL (#105)

  - PROC related tasks:
    - Create Profile (#83)
  
  - User stories (US):
    - Total Effort (#108)

  - Profile:
    - Update member profile (#410)

  - Requirements' designs :
    - Design solution for REQ-131 (#146)
    - Design solution for REQ-190 (#198)

  - Development:
    - Create File class (#245)
