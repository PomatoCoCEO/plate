# André Marques

GitLab ID: @andremarques1

Student Number: 2019218102

![img](./FotografiaWebcam_602970.jpg)

-   20 years old (20/09/2001)
-   Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

-   Hobbies:

    -   Sports (Football, NBA, NFL, F1)
    -   Videogames
    -   Chess


-   Main Roles:
    -   Quality Control Member

-   Activites conducted:
    - Learning or reviewing technologies:
        - Learn Git & GitLab (#16)
        - Learn Django basics (#64)
        - Review Python (#66)
    
    - PROC related tasks:
        - Review test cases proc (#347)
    
    - User Stories (US):
        - View Issue Timeline (#111)
    
    - Requirements' designs:
        - Design solution for REQ-122 (#160)

    - Meetings:
        - Quality control team meeting (07/11/2021) (#267)
    
    - Profile:
        - Create profile folder (#99)
        - Update member profile (#408)
    
    - Requirement testing:
        - Test PlateBoard's REQ - Commits (#412)
    
    - Quality Control:
        - Review and accept merge requests about first requirements designs (#237)
        - Review and accept merge requests about list of commits per file and navbar design (#267)
        - Review and accept merge requests (#315, #360)
