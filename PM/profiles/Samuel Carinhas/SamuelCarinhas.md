# Samuel Carinhas

![img](./SamuelCarinhas.PNG)

- 20 years old
- Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra
- Student number: 2019217199

- ### Hobbies:
    - Saxophone player
    - Competitive Programming

- ### Roles:
    - Backend Leader

- ### Contacts:
    -   DEI Email: carinhas@student.dei.uc.pt
    -   UC Email: uc2019217199@student.uc.pt
    -   Personal Email: samuelsantos.c.2001@gmail.com

- ### Activities Conducted in the Project

    - ### EDU:
        - ### Learning or reviewing technologies:
            - Learn Git & GitLab (#12)
            - Learn Django (#38)

    - ### REQ:
        - ### User stories:
            - Choose GitLab repository (#123)
    
    - ### DES:
        - ### Requirements' Design
            - Design solution for REQ-106 (#151)

    - ### ARCH
        - Create architecture document (#176)

    - ### PROC:
        - Requirements testing template (#386)

    - ### PM:
        - Backend meeting (#200, #281)
        - Managers' meeting (#425)

        - ### Project management:
            - Task distribution for the backend team (Accomplished at backend team meetings)

        - ### Profile:
            - Create user profile document (#95)
            - Update user profile with activities done during the project (#385)

    - ### PROD && ENV:
        - Set up a gitlab runner and make the first deployment (#96)

    - ### DEV:
        - Setup django project (#94)
        - Complete backend API (#202)
        - Fix development branch and create the requirements implementation folder (#316)
        - Implement REQ-108 (#317)
        - Fix the total effort time (#363)
    
    - ### QC:
        - ### Requirement testing
            - Test Plateboard's REQ - Add page that summarizes repo information (#426)
