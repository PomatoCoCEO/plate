# Telmo Quinteiro
Gitlab ID: @telmoquinteiro

![img](./Telmo_Lopes.jpg)

- 23 years old
- Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

- Hobbies:
	- Gaming
	- Music
	- Sports

- Roles:
	- Backend Developer

-Activities Conducted:
  - Learn Git & Gitlab (#10)
  - Learn Django Basics and review Python (#52)

  - PROC related tasks:
    - Create Profile (#82)

  - User stories (US):
    - Content of file on commit (#131)

  - Profile:
    - Update member profile (#409)

  - Requirements' designs :
    - Design solution for REQ-108 (#145)

  - Development:
    - Create Test Cases for Backend-api (#246)
