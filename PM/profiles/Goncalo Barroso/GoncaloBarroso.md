# Gonçalo Marinho Barroso 
**Gitlab ID**: @goncalo_barroso

**Student Number:** 2019216314

![img](./FotografiaWebcam_602791.jpg)

- 20 years old (06/12/2001)
- Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

- **Hobbies:**
  - Workig out
  - Video games
  - Cooking

- **Main Roles:**
  - Front end developer

- **Activities Conducted:**

  - Learning or reviewing technologies:
    - Learn Git & Gitlab (#17)
    - Learn Django Basics (#68)
    - Learn Django Basics Part2 (#101)
    - Python review(#67)
  
  - User stories (US):
    - Back functionality (#122)
   
  - Requirements' designs :
       - Design solution for REQ-111 (#140)
       - Design solution for REQ-181 (#211)
  
  - Profile:
    - Create profile folder (#75)
    - Update member profile (#357)

  - Frontend work:
    - Design of General configurations (#302)
