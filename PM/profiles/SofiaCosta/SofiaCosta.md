# Sofia Costa  

![img](./SofiaCosta.jpg)

Age: 20  
Residence: Coimbra, Portugal  
  
Course: Design and Multimedia  
Student number: 2019209981  
  
### Contacts:
Dei email: sofcosta@student.dei.uc.pt  
UC email: uc2019209981@student.uc.pt  
GilLab nickname: @sofiacosta  

### Hobbies:  
- gymnastics
- reading
- academic tuna

### Role in Project:
- Frontend Developer

### Activities Conducted in the Project: 

#### EDU:
Learning or reviewing technologies:
- Learn Git & Gitlab (#14)
- Review python and PostgreSQL (#55)
- Learn Basics of Django (#56, #72)

#### PROC:
- Create Quality Control team operation document (#166)

#### PM:
Profile:
- Create user profile folder and profile file (#73)
- Update user profile with activities done during the project (#371, #405)

Meetings:
- Weekly team meeting out of class hours (#250, #251)
- Frontend meeting (#210)
- Managers' meeting (#309)

Project Management:
- Plan Frontend team tasks
- Distribution of work (mockup designs) by Frontend team members (#270)

#### REQ:
User stories (US):
- List of Contributions per Member (#117)
Requirements' Mockups:
- Creation of mockup template for guidance (#208)

#### DES:
Requirements' designs :
- Design solution for REQ-109 (#147)

#### DEV:
Frontend work:
- Review of CSS selectors created by Frontend team (#271)
- Creation of html base document (#272)
- Add logo to reposiotry (#305)

#### TEST:
Requirement testing:
- Test DEIshboard requirement - Informação de issues (#411)

#### QC:
- Review of CSS selectors created by Frontend team (#271)
