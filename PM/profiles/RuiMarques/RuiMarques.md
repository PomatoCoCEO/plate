# Rui Marques
#### GitLab nickname: @ruimarques2001

![img](./RuiMarques.jpg)

Age: 20  
Residence: Sobreiro, Condeixa-a-Nova

Course: Computer Engineering in UC  
Student number: 2019216539 

DEI mail: ruimarques@student.dei.uc.pt  
UC mail: uc2019216539@student.uc.pt  

Hobbies:
- Rover of the group 1035 of Scouts in Condeixa
- Practice boxe in AAC Boxing/Kickboxing
- Bladesmith enthusiast and knife collector

Role in Project:
- Frontend Developer

Activities Conducted in the Project: 
- Project Management:
    - Management of presences in meetings
    - Small fixes to the repository
- Organization processes:
    - Creation of file for time management
- Frontend work:
    - Mockup views for Team and Member views
    - Creation and review of CSS selectors
