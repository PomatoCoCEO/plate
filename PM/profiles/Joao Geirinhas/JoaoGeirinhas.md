# João Geirinhas
Gitlab ID: @joao.geirinhas.2001
Student Number: 2019216397

![img](./FotografiaWebcam_602799.jpg)

-   20 years old
-   Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

-   Likes:

    -   Football
    -   Video Games


- Main Roles:

    - Quality Control Member
    - Requirements Team Member


- Activities Conducted:

    - Learning or reviewing technologies:

        - Learn Git & Gitlab (#9)
        
        - Learn Django Basics (#63)

        - Python review(#65)

    - PROC related tasks:
        -  Release Notes document (#387)

    
    - User stories (US):
        - Structure and prioritize requirements (#172)
        - Static File Information (#112)

    - Requirements' designs :
        - Design solution for REQ-110 (#155)

    - Meetings:
        - Quality control team meeting (07/11/2021) (#243)
        - Requirements team meeting (04/11/2021) (#232)
    
    - Profile:
        - Create profile folder (#103)
        - Update member profile (#404)
        
    - Pipeline:
        - Beggining of the project pipeline during a meeting (#243)
    
    - Development:
        - Implement REQ-124 - Category View (#300)
        
    - Requirement testing:
        - Test PlateBoard's REQ - Total Effort (#381)
        
    - Quality control:
        - Review requirements testing document (#388)
        - Review and approve merge requests (#373 #337)
        - Review and correct issues according to the teacher's feedback (#326)
        - Review mockup designs MRs (#325)
        - Review FE team css classes (#299)
        - Review merge requests about the requirements' designs (#162)
        - PROC reviewing (#388)
        - Test REQ-421 (#424)
        - Test and review REQ-108(#381)
