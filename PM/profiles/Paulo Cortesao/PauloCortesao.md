# Paulo Cortesão

![img](./Paulo_Cortesao.jpeg)

-   20 years old
-   Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

-   Hobbies:

    -   Classical Guitar
    -   Competitive Programming

-   Roles:

    -   Project Manager
    -   Backend Developer
    -   Dashboard Architect

-   Activities Conducted in the Project:

1. PM:
    - Presiding group meetings
    - Creating, organising and populating repository (#1, #2)
    - Reviewing meeting minutes (#379, ...)
    - Assigning design-implementation pairs (#193)
    - Distributing testing tasks (#382)
2. EDU:
    - Learning Git and Gitlab (#20)
    - Learning the basics of Django (#31, #77)
    - Creating a video tutorial to teach group members how to work with the Gitlab API (#188)
    - (other activities more related to development, environment and deployment also involved learning)
3. PROC:
    - Creating User Story Template (#134)
4. ARCH:
    - Making decisions about the project's architecture, which would be recorded in a document (#174)
5. REQ:
    - Writing user story for Login Requisite (#107)
6. DES:
    - Designing solution for REQ-118: View all commits of a file and who made them (#136)
7. DEV:
    - Creating a dummy user in the repository for development purposes (#187)
    - Correcting backend API (#256)
    - Implementing repository choice requirement REQ-123 (#257)
    - Formatting dashboard lists in HTML (#336)
    - Formatting several HTML pages in the dashboard (#362)
    - Adding pagination to the social view page (#365)
8. QC
    - Participation in QC team meeting (#244)
    - Testing DEIshbboard's file content requirement (#389)
9. ENV:
    - Implementing and fixing CI pipeline (#261, #279)
10. PROD:
    - Deploying a first version of the dashboard for independent validation using docker (#350)
    - Deploying a second version of the dashboard for testing purposes (#390)
