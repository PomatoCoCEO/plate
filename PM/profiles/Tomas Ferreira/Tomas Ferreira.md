# Tomás Ferreira

**Gitlab ID**: @tomasferreira01

**Student Number:** 2019224786


![img](./tomas.jpg)

-   20 years old
-   Attending the 3rd year of the Bachelor's degree in Informatics Engineering in the University of Coimbra

- **Hobbies:**

    -   Gym

-  **Main Roles:**

    -   Frontend developer
    

- **Activities Conducted:**

  - Learning or reviewing technologies:
    - Learn Git & Gitlab (#25)
    - Learn Django Basics (#89)
    - Review Python and PostgreSQL(#87)

  - PROC related tasks:
    - Release Notes document (#339)
  
  - User stories (US):
    - View contributions per category (#128)
   
  -  Requirements' designs :
       - Design solution for REQ-191 (#197)
       - Design solution for REQ-127 (#159)
  
  - Profile:
    - Create profile folder (#88)
    - Update member profile (#396)
  
  - Development:
    - Implement REQ-109 - Events Organized by Commit Instants (#318)

  - Requirement testing
    - Test DEIshboard's REQ - Issue List (#407)

  - Frontend:
    - Design search-bar and white (#266)
    - Design of other(#288)
    - Design of issues view (#278)
    
