# **Independent Validation Document - Deishboard**

Project to test: **DEIshboard** (PL1)

Testing team: **Plateboard** (PL2)

Public version link for the dashboard: [deishboard.herokuapp.com](https://deishboard.herokuapp.com/))

Dashboard version: **1.0**

Version date: **December 4th, 2021**

Validation date: **December 18th, 2021**

Requirements repository page: **[https://gitlab.com/pl110/deishboard/-/blob/main/%5BDocumentos%5D/%5BREQ%5D%20Requisitos/Entrega0412-RequisitosDetalhados.md](https://gitlab.com/pl110/deishboard/-/blob/main/%5BDocumentos%5D/%5BREQ%5D%20Requisitos/Entrega0412-RequisitosDetalhados.md)**


This document aims to describe the tests conducted by some Plateboard members on the Deishboard platform ([deishboard.herokuapp.com](https://deishboard.herokuapp.com/)), version 1.0 from 04/12/2021. The testing for each requirement presented by DEIshboard was assigned to a group member in Plateboard. After having conducted the tests and retrieving the results, it was considered that the list of implemented requirements is actually shorter than what was presented, as some requirements failed all tests. As such, the dashboard is **not approved** and should be more carefully reviewed.


The test distribution is as follows:

- Page with list of active members of the repository - **Rui Marques**
- Observe each member&#39;s profile page - **Vicente Tiago**
- Authenticated users with exclusive access - **André Figo**
- View files by category - **João Artur**
- File content view - **Paulo Cortesão**
- Issues list - **Tomás Ferreira**
- Issues info - **Sofia Costa**
- Issues concluded by area - **Joana Simões**
- Loading page - **Gonçalo V Correia**

The list below contains the test cases and results to validate the team&#39;s software.

## **Page with list of active members of the repository**

The student **Rui Marques** was responsible for testing the page with the list of active repository members.

### **Tests:**

1. Check if the list of members is displayed in the page;
2. Check for each member if he/she has made at least one commit in the repository

To pass this test the page has to count the active members of the repository. To be an active member means to have at least one commit in the repository. 
### **Results:**

1. ✔ All members displayed in the page have, at least, one commit made, making them active members.
2. ✘ The Deishboard only contains its own repository. Therefore, it is impossible to test if all members displayed could be active members.

It should be noted that if there was access to different repositories, preferably public ones, it would be better to test if users with no commits are listed in the active members list.

## **Observe each member&#39;s profile page**

The student **Vicente Tiago** was responsible for viewing a profile page for each team member. To accept the profile page, some criteria are used, namely: Member information page (name, role in the project, age, email and course), a bar graph with information about the number of working hours organized by dates, the number of issues organized by the different areas, time spent (in hours) history organized by weeks, total time spent by member as well as average weekly hours.

### **Tests:**

1. Check if every member has a profile page with all the information listed in the acceptance criteria.
2. Check if the total time spent by the team listed in the dashboard is equal to the sum of the time spent by each member.
3. Check if the graphs are in the right place.
4. Check if the total time spent has the same format in each member profile page.
5. Check if the average weekly hours spent by member has the same format in each member profile page.


### **Results:**

1. ✔ It was possible to see all the information listed in the acceptance criteria by member profile page.
2. ✔ The total time spent by the team listed in the dashboard was equal to the sum of the time spent by each member.
3. ✘ None of the graphs respected his space. For example the time value was not exactly under a bar and there were bars in the middle of two values.
4. ✔ The total time spent had the same format in each member profile page
5. ✘ The average weekly hours spent by member didn&#39;t have the same format in each

It should be noted that the values has no units, it should be hours and was a member where the time value was not rounded to the hundredths.

## **Authenticated users with exclusive access**

The student **André Figo** was responsible for testing this feature. The acceptance criterion for this requirement is that it is only possible to access the page after filling in the login fields. To check that everything is working properly, some tests were performed, such as:

### **Tests:**

1. Fill in the login fields with a valid gitlab account data and press login and check if you can see the repository;
2. Repeat test 1 with invalid data and check if you can see the repository;
3. Repeat test 1 with no data and check if you can see the repository;
4. Check if different accounts open up different repositories;
5. Use the link [https://deishboard.herokuapp.com/deishboard/](https://deishboard.herokuapp.com/deishboard/) to check if the system is secure and won&#39;t bypass the login.


### **Results:**

1. ✔ It was possible to enter in the repository with valid data on the login fields. However, there is only one repository available and even accounts with no access to that repository can view its information;
2. ✘ It was still possible to enter the repository with invalid data in the login fields; The same repository was shown;
3. ✘ It was still possible to enter the repository with invalid data in the login fields; The same repository was shown;
4. ✘ There is only one repository available and different accounts with no access to that repository can view its information displayed;
5. ✘ It was possible to enter the main repository page through the link, without filling in the login fields;

It should be noted that when trying to access the repository, in the login page, sometimes an &quot;Application error&quot; appears. It was concluded that the login is not working, because anyone can access the repository information even without an account.

## **View files by category**

The student **João Artur** was responsible for viewing files by category. The acceptance criteria presented for this requirement were having directories matching the different categories and see which files exist in the directories. To check if the views were correct, the collaborator did some tests:

### **Tests:**

1. Check if all the categories have a corresponding directory;
2. Visualize which files exist in a certain directory;
3. Visualize the files contained in a directory of a certain certain category, select a different category and see correct information;
4. Refresh the page several times, while passing the mouse cursor through different page elements, and see if the root contents presented are consistent.


### **Results:**

1. ✘ Without a proper definition of what was considered a category, after analyzing the file organization, it was concluded that a category is considered to one of the directories listed on the REPO skeleton released by the theoretical classes professor. Despite not following the visible conventions, it was understood that the folder &quot;Regras e Processos&quot;, in the QA category, corresponds to the PROC category, thus not being the motive for failure. However, a folder matching the category PROD was not found and it wasn&#39;t verified the previous situation for the PROC category.
2. ✘ It is not possible to see which files are contained in some directories. Following the path &quot;[DEV] Desenvolvimento/[FE] Frontend/[DES] Design&quot;, it is not possible to see the files which are inside the [DES] Design folder. Other instances of this situation were encountered.
3. ✘ After changing the category, files from the previous category are still presented. To reproduce this result, go to the directory &quot;[ARCH] Arquitetura Projeto/Anteriores&quot; and, for example, select the DEV category. The files contained in the folder &quot;Anteriores&quot; are still displayed.
4. ✔ The contents are consistent.

It should be noted that the team profile files overflow the space available for the display. Despite the test 4 being considered correct, it was noticed that sometimes other contents have a flash appearance when reloading. Due to not understanding the nature of this event, it was not considered as a motive for failure.

## **File content requirement**

The student **Paulo Cortesão** was responsible for testing the file content view. The acceptance criteria for this requirement is that the various files present in the team&#39;s repository can be accessed through the page &quot;Files&quot;.

### **Tests:**

1. Access the site in the &#39;Files&#39; page and try to navigate around DEIshboard&#39;s repository, using the graphical interface available. Verify that the files which were found are correct.


### **Results:**

1. ✘ A preview of a file called README.md, containing the names of the representatives of the various subteams, appears on the screen. The file&#39;s history, path and related issues are also displayed. When hovering the mouse over the preview, the preview of another file (the minutes of a meeting with the client) appears, as well as this file&#39;s history, path and related issues. With the knowledge that was provided by the team; and bearing in mind the page layout, it was impossible to obtain the content of files (and respective metadata) other than from these two. As such, and bearing in mind that this repository must also have Python code and HTML/CSS files to support it, it was inferred that not all files are covered by this functionality. Because of this, the software failed the test case.

Considering the results produced by the tests, it was concluded that the functionality present in the software does not suffice to complete the requirement. As such, this requirement is considered incomplete.

## **Issue List**

The student **Tomás Ferreira** was responsible for testing the issue list. To accept this requirement it&#39;s necessary to test the acceptance criteria, in particular the possibility of seeing a list with the open issues and of obtaining information about the issue selected. Some tests were conducted:

### **Tests:**

1. Open the Deishboard&#39;s issue page.
2. Open the list of open issues.
3. Open an issue that is with open status.
4. See if the information is according to the title of the issue.

After running the tests, were made some notes of the results of the tests:

### **Results:**

1. ✔ When going to the issue page there is a list of issues.
2. ✘ There is a list of open issues and another list of close issues.
3. ✘ There is a filter to see if the issues are open or closed.
4. ✘ When clicking on an issue it is possible to see the issue content.

## **Issues Information**

The student **Sofia Costa** was responsible for testing the issue info. In order to accept this requirement, the issue description needed to be visible, and it should have the assignee name, the history of the actions done on the issue and show time tracking.

### **Tests:**

1. Open the Deishboard&#39;s issue page
2. See if the information is according with the title of the issue
3. Check if issue&#39;s description is visible
4. Check if assignee name is present
5. Check if issue&#39;s history is present
6. Check if time tracking is showing

### **Results:**

1. ✔ It is possible to open the issues page
2. ✘ It is not possible to open a specific issue, we can only see one issue&#39;s information
3. ✔ It is possible to see the description of the issue (even though we can&#39;t change the issue)
4. ✔ It is possible to see the assignee name
5. ✔ It is possible to see the history of the issue
6. ✘ It is not possible to see the time tracking, we can just see the title, the estimate and the spent time

It should be noted that it is not possible to change the issue: we can only see one issue description, assignee, history and time tracking, and even after refreshing, we always see the same issue.

## **Issues completed by category requirement**

The student **Joana Simões** was responsible for testing the issues concluded by area. For the acceptance of this requirement, the software needed to display the number of issues completed by category in a pie chart, as well as the total number of issues.

### **Tests:**

1. Open the repository and check if the pie chart with the number of issues concluded per category can be seen;
2. Open the repository and check if the number of total issues can be seen;
3. Resize the screen and check if the result in points 1 and 2 are still valid;

### **Results:**

1. ✔ The pie chart with the number of issues concluded per category is visible on the homepage;
2. ✔ The total number of issues are shown on the homepage;
3. ✘ When resizing the screen to the smallest (horizontally), the components disappeared with no option to scroll and view them.

It should be noted that when trying to access the repository, in the login page, sometimes an &quot;Application error&quot; appears, and although tests 1 and 2 are right for the given repository, it was impossible to confirm whether the values would be accurate in other repositories.

## **Loading Page**

The student **Gonçalo V. Correia** was responsible for testing the Loading Page. In order to be accepted, the requirement should have a loading animation and everytime the page loads this animation must show up and on the other hand, when the page is loaded this animation disappears.

### **Tests:**

1. Open the Deishboard&#39;s login page
2. Open &quot;Home&quot; by clicking on it
3. Open &quot;Repositório&quot; by clicking on it
4. Open &quot;Ficheiros&quot; by clicking on it
5. Open &quot;Tarefas&quot; by clicking on it
6. Open &quot;Social&quot; by clicking on it
7. Open &quot;Home&quot; by changing the url
8. Open &quot;Repositório&quot; by changing the url
9. Open &quot;Ficheiros&quot; by changing the url
10. Open &quot;Tarefas&quot; by changing the url
11. Open &quot;Social&quot; by changing the url
12. Going back to the previous page


### **Results:**

1. ✘ When going to the login page there is no loading animation.
2. ✔ There is a loading animation and when the page is loaded the animation disappears.
3. ✔ There is a loading animation and when the page is loaded the animation disappears.
4. ✔ There is a loading animation and when the page is loaded the animation disappears.
5. ✔ There is a loading animation and when the page is loaded the animation disappears.
6. ✔ There is a loading animation and when the page is loaded the animation disappears.
7. ✘ There is no loading animation.
8. ✘ There is no loading animation.
9. ✘ There is no loading animation.
10. ✘ There is no loading animation.
11. ✘ There is no loading animation.
12. ✘ Sometimes the loading animation takes a few seconds to pop up, and then it stays there forever. Page is never loaded.

It should be noted that in the REQ user story, &quot;higher time&quot; should be more explicit.

## Conclusion

Considering these results, it is clear that this dashboard has bugs needing to be addressed. It was possible to make each requirement fail at least a test case, which means that the usability of this software would greatly benefit from a thorough review.

---

<sub>

**Writers**: Gonçalo Correia and João Garcia 

**Reviewers**: André Figo and Paulo Cortesão

17/12/2021
</sub>

