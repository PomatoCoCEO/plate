# Project Architecture

This document contains the main guidelines regarding the project's architecture. They should be followed throughout the software development.

## High-level view

A high-level view of the project's architecture is shown here:
<img src="arch_high_level.svg">

<sub>(generated in PlantUML, source code: arch_high_level.puml)</sub>

The application running in the web browser should bear a copy of the repository to analyse. This copy should be saved in the browser's cache; it should be built as information is being requested by the client; and it should make the access to the various elements to display easy.

## Structure of the repository clone

The repository clone to work with should be divided into interrelated sections:

<img src="repo_structure.svg">
<sub>(source code: high_level_repo.puml)</sub>
These sections ought to be subdivided into relevant parts, bearing in mind the following requisite dependency graph:

<img src="req_dep_graph.svg">
<sub>(source code: req_dep_graph.puml)</sub>
