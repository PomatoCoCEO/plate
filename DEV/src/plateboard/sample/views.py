'''
Module meant to exemplify a deploy test
'''
from django.http import HttpResponse


def welcome():
    '''
    Tests deployment
    '''
    return HttpResponse('Deploy test')
