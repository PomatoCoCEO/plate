"""
Script for testing the cc rating: git pull origin
Usage: radon cc *.py > (file to test) && python3 (file to test)
"""
import sys

c = 0
if __name__ == "__main__":
    a = sys.argv[1]
    b = []
    o = open(a, "r")
    b = o.readlines()
    for l in b:
        if l[0] != " ":
            continue
        if l[len(l) - 2] != "A":
            print("Problem: %s" % l, end="")
            c += 1
    if c != 0:
        sys.exit("%d problem%s encountered" % (c, "" if c == 1 else "s"))
