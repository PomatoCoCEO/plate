# runs through all directories, recursive
# 1st argument: local, starts with '.'
# 2nd argument: regex
# normal use: ./dir_runner.sh . <regex>
# regex for python files: ^.*\.py$
# to enumerate: for a in $(./Scripts/dir_runner.sh . ^.*\.py ./Scripts); do echo $a; done
for f in $(ls  $1); do
    # echo "$1/$f"
    if [ -d "$1/$f" ]; then
        $3/dir_runner.sh "$1/$f" $2 $3
    else
        if [[ $f =~ $2 ]]; then 
            echo "$1/$f"
        fi
    fi
done