"""
Script for testing the cc rating: git pull origin
Usage: radon cc *.py > (file to test) &&
    radon mi *.py >> (file to test) &&
    python test.py (file to test)
"""
import sys

C = 0
if __name__ == "__main__":
    a = sys.argv[1]
    b = []
    o = open(a, "r", encoding="ascii")
    b = o.readlines()
    for l in b:
        if l[-4:-1] == ".py":
            continue
        if l[-2] != "A":
            print(f"Problem: {l}", end="")
            C += 1
    if C != 0:
        sys.exit("{} problem{} encountered".format(C, "" if C == 1 else "s"))
